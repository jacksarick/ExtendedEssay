trend = []
prev = 0
with open(raw_input("File\n-> ")) as file:
	next(file)

	for line in file:
		line = map(float, line.split(", "))
		trend.append(["Down", "Up"][line[1] > prev])
		prev = line[1]

text_file = open("output.txt", "w")
text_file.write("\n".join(map(str, trend)))
text_file.close()