import matplotlib.pyplot as plt

labels = [26.5, 26.5, 26.5, 35, 36, 56, 57, 57.5]
averages = [-8.755829334001285e-09, 2.842917118476908e-09, 1.4034909825907418e-09, -1.6854906564380414e-09, -6.495862930786365e-08, -4.6501037746638773e-08, -6.405263626551995e-08, -4.977793139466718e-08]
x = range(len(averages))
plt.plot(sorted(averages)[::-1])
plt.plot([0 for k in x], color="red")
plt.title("Difference in Time vs. Temperature")
plt.xlabel("Temperature (C)")
plt.ylabel("Average Time Lost per Second")
plt.xticks(x, labels)
plt.show()