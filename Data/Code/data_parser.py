import matplotlib.pyplot as plt

data = []
filtered = []
averages = []

for file_name in raw_input("Files\n-> ").split(" "):
	try:
		with open(file_name) as file:
			next(file)

			for line in file:
				line = map(float, line.split(", "))
				data.append(line)

		for point in range(2, len(data)-3):
			if ((data[point][1] > data[point-2][1]) and (data[point][1] > data[point+2][1])) or ((data[point][1] < data[point-2][1]) and (data[point][1] < data[point+2][1])):
				filtered.append(data[point])

		plot_x = [d[0] for d in filtered]
		plot_y = [d[1] for d in filtered]
		distances = [plot_y[n] - plot_y[n+1] for n in range(len(plot_y)-2)]
		averages.append((sum(distances) / len(distances))/max(plot_x))

	except Exception, e:
		print "Failed file " + file_name + " with error " + str(e)

labels = [26.5, 26.5, 26.5, 35, 36, 56, 57, 57.5]
x = range(len(averages))
plt.plot(averages)
plt.title("Difference in Time vs. Temperature")
plt.xlabel("Temperature (C)")
plt.ylabel("Average Time Lost per Second")
plt.xticks(x, labels)
plt.show()