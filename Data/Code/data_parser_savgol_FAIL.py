import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

averages = []

for file_name in raw_input("Files\n-> ").split(" "):
	try:
		# Get the file I'm working with
		experiment_name = file_name.rstrip(".csv").replace("_", " ")

		# Make a function to plot easier
		def easy_plot(x, y, title, experiment=experiment_name, color_in="blue", xlabel="Time", ylabel="Voltage"):
			plt.plot(x,y, color=color_in)
			plt.xlabel(xlabel)
			plt.ylabel(ylabel)
			plt.title(title)
			plt.savefig(experiment.replace(" ", "_") + "_" +  title.replace(" ", "_") + ".png", bbox_inches='tight')
			plt.close()

		# Read in my data
		with open(file_name) as file:
		    next(file)
		    data = [x.split(", ") for x in file]

		# Split into component parts
		x = map(float, [point[0] for point in data])
		y = map(float, [point[1] for point in data])

		# Apply Savitzky-Golay filter
		yhat = savgol_filter(y, 51, 3) # windowd size 51, polynomial order 3

		# easy_plot(x, y, "Raw Data", color_in='red')
		# easy_plot(x, y, "Savitzky-Golay Filter", color_in='blue')

		# Find peaks and troughs
		peak = trough = []

		for point in range(1, len(yhat) - 2):
			if (yhat[point] > yhat[point-1]) and (yhat[point] > yhat[point+1]):
				peak.append((yhat[point], x[point]))

			if (yhat[point] < yhat[point-1]) and (yhat[point] < yhat[point+1]):
				trough.append((yhat[point], x[point]))


		# Make an overly smoothed graph
		distances = [point[1][1] - point[0][1] for point in zip(peak, trough)]
		averages.append(sum(distances)/len(distances))

	except Exception, e:
		print "Failed file " + file_name + " with error " + str(e)

easy_plot([26.5, 35, 56, 26.5, 37, 57.5, 26.5, 36, 57],
	averages,
	"Change in Signal Variance",
	xlabel="Temperature (C)",
	ylabel="Time between Peak-Trough set")