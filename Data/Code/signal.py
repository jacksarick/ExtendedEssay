import matplotlib.pyplot as plt

data = []
filtered = []

with open(raw_input("Files\n-> ")) as file:
	next(file)

	for line in file:
		line = map(float, line.split(", "))
		data.append(line)

	for point in range(2, len(data)-3):
		if ((data[point][1] > data[point-2][1]) and (data[point][1] > data[point+2][1])) or ((data[point][1] < data[point-2][1]) and (data[point][1] < data[point+2][1])):
			filtered.append(data[point])

	plot_x = [d[0] for d in filtered]
	plot_y = [d[1] for d in filtered]
	distances = [plot_y[n] - plot_y[n+1] for n in range(len(plot_y)-2)]

x = range(len(distances))
plt.plot(distances)
plt.title("Difference in Time")
plt.xlabel("Peak-Trough set number")
plt.ylabel("Difference in Time")
plt.show()