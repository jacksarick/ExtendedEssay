import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
from scipy.signal import savgol_filter

# Uniform Data
N = 100
x = np.linspace(0,2*np.pi,N)
y = np.sin(x) + np.random.random(N) * 0.2

# Convolution Method
def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

# plt.plot(x, y, color="red")
# plt.plot(x, smooth(y,15), color="blue")
# plt.title("Convolution Method")
# plt.show()

# Discrete Fourier Transform Method
w = scipy.fftpack.rfft(y)
f = scipy.fftpack.rfftfreq(N, x[1]-x[0])
spectrum = w**2

cutoff_idx = spectrum < (spectrum.max()/5)
w2 = w.copy()
w2[cutoff_idx] = 0

y2 = scipy.fftpack.irfft(w2)

# plt.plot(x,y, color="red")
# plt.plot(x,y2, color="blue")
# plt.title("Discrete Fourier Transform Method")
# plt.show()


# Savitzky Golay Method
yhat = savgol_filter(y, 51, 3)

# plt.plot(x,y, color='red')
# plt.plot(x,yhat, color='blue')
# plt.title("Savitzky Golay Filter Method")
# plt.show()

#Overlay
plt.plot(x,y, color='black', label="Original")
plt.plot(x, smooth(y,15), color="blue", label="Convolution Method")
plt.plot(x,y2, color="green", label="DFT Method")
plt.plot(x,yhat, color='red', label="SavGol Method")
plt.legend()
plt.title("Overlay of methods")
plt.show()