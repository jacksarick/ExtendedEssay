import numpy as np
import matplotlib.pyplot as plt
averages = sorted([-8.755829334001285e-09, 2.842917118476908e-09, 1.4034909825907418e-09, -1.6854906564380414e-09, -6.495862930786365e-08, -4.6501037746638773e-08, -6.405263626551995e-08, -4.977793139466718e-08])[::-1]


x = np.array([26.5, 26.5, 26.5, 35, 36, 56, 57, 57.5])
y = np.array(averages)
z = np.polyfit(x, y, 3)
p = np.poly1d(z)
xp = np.linspace(-10, 70)

plt.plot(xp, p(xp), color="green")
plt.plot(x,  y,     color="blue")
plt.title("Difference in Time vs. Temperature")
plt.xlabel("Temperature (C)")
plt.ylabel("Average Time Lost per Second")
plt.show()