#Resources

##Materials

###Owned
- Arduino Uno (technically Chinese clone)
	- Also Arduino nano, unlikely it will be needed
- Breadboard/Jumper wires
- 4MHz Crystal oscillator (4 pin)
- 4MHz Ceramic resonator
- Various other parts

###Needed
- More oscillators
	- Various frequencies
- More resonators (?)


##Tools
- Oscillator (>30MHz resolution)
- Multimeter (I have a small one that works well on PCBs)
- Something to heat small items up to specific temperatures (I was thinking hotplate or Bunsen burner)
- Thermometer (Must be able to read the surface of a small component)
- Computer for logging data
	- Most likely using my mac, but if it is not looking like a good environment for a laptop, I can log the data on an OLPC or a headless Pi.