##Abstract
This paper looks at the decay in frequency stability of a quartz crystal oscillator at non ideal temperatures, and how to counteract that effect without controlling temperature. As a piezoelectric material, in this case quartz crystal, is heated, the frequency it vibrates at becomes less and less regular. Because the decrease in frequency is not the same across all crystals, it can be hard to correct accurately. Correcting the frequency of individual crystals has been abandoned in favour of using multiple crystals and controlling their frequency quite carefully. As an alternative approach, it is possible to use a third-degree polynomial to produce a more regular signal. 

##Introduction
Piezoelectricity /piˌeɪzoʊˌilɛkˈtrɪsɪti/ is the effect observed when certain materials produce an electric charge when mechanical force is applied. It is also a reversible effect, meaning that a piezoelectric material will produce a mechanical force in response to an applied electric field. In most cases, the mechanical force generated tends to be in the form of a vibration. Bone, some ceramics, most crystals, and DNA are all notable piezoelectric materials that show up in day-to-day life. Despite it's fascinating properties, piezoelectricity didn't get any attention from the scientific community when it was discovered in 1880 by the Curie brothers. Only in World War I was there a practical application for piezoelectric materials, when Paul Langevin (Pierre Curie was his doctoral advisor[^curie]) used quartz crystals to accurately detect submarines underwater[^history]. 
<br>
###Modernity
From WW1 to now, the uses increased exponentially, with piezoceramics finding their way into every microphone, radio, and cars across the world. The practical application of this effect are virtually endless. Its high precision lends itself to delicate sensors such as microbalances[^ubalance] for measuring mass and viscosity, and strain gauges[^straing] for measuring how much force is being applied to surface. A piezoelectric motor moves in tiny increments making it quite accurate, though they still generate enough force to move large objects[^LASER] (relative to their tiny size). There are hundreds more applications for this effect, and more being discovered regularly[^future]. Most people know a much more common application, so common I'm willing to bet that it's on your wrist. Quartz watches are named for the sliver for quartz crystal that keeps the time inside them. For the most part, they keep a very precise tempo, oscillating anywhere from 4 million times per second to 60 million depending on the circuit. At room temperature running for a decade straight, a quartz watch will normally be about a third of a second off. This makes them great for consumer technology that is designed to remain room temperature, such as a watch. 

###Problems
There are always edge cases, however. In hot environments the piezoelectric effect becomes less consistent, so a baker or welder may lose about 3 seconds every year if they wear their watch to work. In an electrical circuit where the timing circuit is too close to a hot component, it could lose 3 minutes a year, or more if the circuit is left to run for extended periods of time. On the other end of the spectrum, a quartz crystal cooled to -200º will oscillate approximately 325% faster[^mad_science]. These cases may seem unlikely or unnoticeably small, but consider the extremes. On board every submarine, spaceship, and snowplow is a small device to keep time. It cannot be mechanical because it needs to last for a while without maintenance, yet it is difficult to make a small, cheap, low energy timing circuit that works and different temperatures.

### How the piezoelectric effect works
***CANNOT WRITE RIGHT NOW*** This means that correcting the decay in frequency is impossible by simply modulating the current or potential difference across the crystal. A mechanical solution would fail too, as engineering something that small and precise is not currently possible. This leads us to the research question: How to stabilize the frequency of a crystal oscillator without controlling the temperature of it? _____ posed that third degree polynomial might be fit to a frequency vs. temperature graph of the crystal ***GO ON***

### Hypothesis
By measuring the frequency of a crystal at multiple temperatures, it is possible to generate an equation that can accurately correct the frequency based on the temperature of the crystal. This equation can be put on a small chip to filter the output of the signal and produce a regular signal.

##Initial Testing and Design

###Oscillator
Before attempting to run the full experiment, I first ran a series of small experiments to verify everything was functioning the way it should. The first of many small experiments was to make sure to the crystals produced the correct signal. To do this I simply wired a low-profile 4 MHz crystal to the y-axis of a 100 MHz oscillator[^crystal_1]. This produced a very nice sine wave, with a little background noise from the oscillator[^oscillator_1]. In an attempt to reduce this, I used an external power source to generate the current[^crystal_2], but it didn't really help, because I couldn't correctly calibrate the oscillator to deal with it.

###Heating Element
This was a topic of discussion for a good chunk of time between the lab supervisor and myself. The on board crystal of the Arduino is 4.5mm x 9.5mm[^crystal_mm], so I had to find a method that would be

1. Small
2. Constant Temperature
3. Variable Temperature

My initial thought was to use the tip of a soldering iron, but it was much too hot, and would have desoldered parts of the Arduino. The winning idea ended up being fastening a resistor to the top of the crystal, and connect it to a variable DC power source. To determine the resistance needed, a large resistor (R1) in was put in parallel to a rheostat (R2), and measured the potential difference and current of the circuit[^rest_pic]. <center><img style="width: 50%;" src="./pics/diagram.jpg"><br>**FIG 1:** A diagram of the circuit used to calibrate the heating resistor</center>
<br>
From there, a temperature probe was wrapped around resistor[^rest_temp] (R1). I chose to use an infrared thermometer to measure the surrounding temperature, because our model could measure from the infrared sensor and the probe simultaneously (and it was really fun to use). Through a bit of trial and error (there were also very few resistors to choose from), a 46Ω±10% resistor was chosen. When it was the only load in the circuit, it heated up approximately 10ºC per volt.

###Circuit Design
In theory, connecting the oscillator in series with a simple load to control the current should produce a regular frequency. 

##Experiment

###General Environment
Within reason, the surrounding environment doesn't matter since all relevant factors will be controlled. The temperature of the crystal will need to be kept at a constant temperature, but that does not require the surrounding environment to be kept at a constant temperature to get an accurate reading.

###Setup
There are two ways to set up the experiment, depending on the resources at hand. With a good oscilloscope that has the capability of recording data, it is possible to record the output of the crystal directly. Unfortunately, an oscilloscope like that tends to cost \$500 at the very least. Instead, a timing circuit built around the crystal designed to emit a regular signal based on the crystal's time. The circuit, along with an interface to connect it to a computer, can be made for around \$15.

###With high-frequency equipment
1. Run some amount of current through the crystal (depends on kind of crystal)
2. Affix a heating element to the crystal
3. Log the potential difference across crystal using equipment that has a higher recording frequency than the crystal 
4. Turn the heating element on and let it warm up
5. Log the frequency again

###With low-frequency equipment
1. Build a circuit that uses a crystal as the timer, to reduce the frequency.
2. Program the circuit to pulse at a regular interval
3. Affix a heating element to the crystal
4. Record output of circuit using equipment operating that has a higher frequency and is more accurate  
5. Turn the heating element on and let it warm up
6. Log the frequency again

The setup I went with was an Arduino attached to a Vernier™ differential potential difference probe[^probe], recording the data with LoggerPro. An Arduino is an ATMega238p microcontroller on a board with a programmer, some IO pins, two ports for power and programming, but most importantly, it uses a 12 MHz quartz for it's timing circuit[^ard_time].


##Interpreting Data

Each trial ran for 5 seconds, sampling at 100,000 times a second. Since there were 9 trials $5 * 100,000 * 9 = 4,500,000$ so there were 4 million data points after all the garbage data had been pared out. To change the data from a set of potential difference across the Arduino vs. time is conceptually easy, but hard in practice. In practice, the data isn't ![](./Data/graphs/Trial_3_0V@26.5_Raw_Data.png)<center>**FIG 2:** A graph of potential difference across the Arduino vs. time. This was the data that was collected by the computer</center> <br>The information is simply too dense to see anything, though there are is one small feature that is visible right from the start. Underneath the large potential difference changes, a faint sub-pattern is visible. This sub-pattern is only seen when zoomed quite far out from the graph and doesn't show up again in any of the later data. I don't believe further investigation would reveal it to be anything except an optical illusion.

###Data Smoothing
I initially thought I would need to smooth the data, but as discussed later, this was a major error on my part. I still documented the whole process because it did reveal some other interesting features about the data. The following is my ***Thoughts*** at the time: The sensor used was quite accurate, both in timing and in data. The Arduino, however, was not accurate, and had trouble with rapid potential difference changes. Since the entire experiment was all about potential difference changes, the data collected was too noisy to work with. If it had been a straight line, or even a parabola, LoggerPro or Excel could've used a simple line-of-best-fit algorithm, but this was a sine wave. There were three algorithms that can be used for smoothing out this kind of data:

1. Convolution
2. Discrete Fourier Transformation (DFT or FFT)
3. Savitsky-Golay Filter (SavGol)

In an effort to decide which one to use, a table of pros and cons was made for each of the smoothing methods[^table]

Convolution | DFT | SavGol
--- | --- | ---
**Pros:** <br> Cheap <br> Simplest| **Pros:** <br> Fast <br> Smooth | **Pros:** <br> Accurate <br> Smooth
**Cons:** <br> Slow <br> Not Smooth| **Cons:** <br> Complex <br> Inaccurate | **Cons:** <br> Slow <br> Complex <br> Expensive

As a test, a sample of data was run through each method. ![](./Data/Smoothing/Comparison.png) <center>**FIG 3:** Side-by-side comparison of all four data smoothing methods</center> <br>Between the pro-cons table and the comparison, choosing was easy. DFT was fast, and produced a very smooth curve, yet it had to dip below the lowest data point, and couldn't go above it. Between he Savitsky-Golay and convolution methods, I went with Savitzky–Golay because it produced a nicer curve. Despite being the slowest, it was the one settled on. 

> A Savitzky–Golay filter is a digital filter that can be applied to a set of digital data points for the purpose of smoothing the data, that ... This is achieved, in a process known as convolution, by fitting successive sub-sets of adjacent data points with a low-degree polynomial by the method of linear least squares.[^wikipedia]

Note that it does use convolution, but it uses it in parallel with a series of other equations. Both Savitzky-Golay and convolution relay heavily on low-degree polynomials, something that comes up later in the paper. This does explain why the Convolution and Savitzky-Golay plots look similar. In both filters, the goal is to find a low-degree polynomial that fits a small window of the data, however the Savitzky-Golay filter later joins the smaller equations together, while convolution does not. I am not even close to qualified to write my own Savitzky-Golay filter, so I used SciPy's function[^scipy]. All of the code for it is open source, so I double checked it and it was exactly what I needed. Here is the smoothed out version of the above graph. ![](./Data/graphs/Trial_3_0V@26.5_Savitzky-Golay_Filter.png)<center>**FIG 4:** The raw data after being passed through the Savitsky-Golay Filter</center> <br>Unsurprisingly, it looks almost exactly the same. The data that was being smoothed out was tiny, so small the computer had trouble graphing it. That fact, combined with the sheer mass of data, means that it is impossible to visualize any meaningful data. The only way I was able to interface the data to a human being was to convert it to sound. If you listened closely, you could hear the imperfections in the sound, but it was too grating on the ears to listen for more than four or five seconds. 

On reflection, smoothing the graph with a Savitzky-Golay filter, or most other filters, is the fact that they smooth out the data too much. In the case of Savitzky-Golay, it generates a series of equations that fit a point and it's neighbours well. Since all of the data was quite similar and computing power was relatively low, the filter produced curves too similar to detect a difference in.

###Frequency Detection

Since smoothing the data out at the start did not work, another approach was needed. Human beings tend to be really good at seeing things, especially seeing patterns like frequency. Unfortunately, the data included a quarter of a million peaks and troughs, so it would take a while to record all of them if a human did it. Computers don't have the ability to do this by themselves. To detect the peaks and troughs in the rough data, a small bit of code was used to pull any point where the three points on either side of it were all above or below it. The logical representation would be similar to `((p > n) and (p > k)) or ((p < n) and (p < k))` where `p` is the test point, and `n` and `k` are the points on either side of `p`. This creates a set of data that represents the actual frequency of the crystal. Taking the average of the di. To turn it into time lost/gained per second, simply divide it by the time the given trial ran for.
![](IDFK.png)<center>**FIG 5:** The distance in time between each peak trough-set.</center> <br>There appears to be some sort of pattern that repeats at a regular interval (about once every 10,000 pairs), but it is too faint to actually determine the pattern and as a result, too difficult determine its source. My best guess is it that it has something to do with the sample rate of the probe overlapping with the frequency of the crystal.

###Correcting
Now that we have the average time difference, we can plot difference in time against temperature to see the decline in frequency stability. The blue line is the data, the red line is "ideal" (frequency is the same regardless of temperature) ![](Average_Time_Lost_per_Second.png)<center>**FIG 6:** Average time lost per second is the amount of time that the timing circuit was wrong by at each temperature</center> <br>It's not a perfect curve by any measure, but it fits as well as can be expected. Looking at the graph, it is easy to see an exponential decline. This means that the graph of the frequency is going to be a polynomial of the second degree or higher, but because the trend never changes direction it has to be a third degree polynomial. Once the degree of the equation is known, a curve can easily be fit to the data. In this case, numpy[^numpy] was the tool of choice for polynomial fitting, because polynomial interpolation is difficult to do without a background in math. For this graph, where `x` is temperature and `y` is time offset our equation looks like this:

$f(x) =(-8.75*10^{-12} *x^3) + (1.097*10^{-9}*x^2) - (4.582*10^{-8}*x) + 6.081*10^{-7}$

Plotting it on top of our data, it is quite a good fit, and accurately reflects what was predicted
![](curve_fit.png)<center>**FIG 7:** Data collected graph on top of the  generated equation</center>
<br>As expected, the extremes of heat and cold are in opposite directions, indicating a loss of time in the cold direction (speeding up), and a gaining of time in the hot direction (slowing down). Verifying that the curve and the data look similar shows that correcting the data with that equation returns a correct measurement of time. Running the data back through the equation produces a flat line, proving that the signal is constant.

###Relating to smoothing
It is interesting to note that the polynomial fit function implemented is the same one used in the Savitzky-Golay filter. The Savitzky-Golay was by all means a mistake on my part, it wasn't totally misguided. A Savitzky-Golay could theoretically be applied as an alternative to a 3rd degree polynomial, and it might even do a better job in some cases. For it to do a better job than a polynomial would require a much longer calibration process, and would drastically increase the memory requirements of the system processing the crystal's signal.

##Conclusion

The method used in this paper can be used to determine the stabilization equation for any piezoelectric material

1. Measure material against stable timer three temperatures with at least 10º C difference between each
2. Find average peak-trough difference
3. Plot peak-trough against temperature
4. Fit 3rd degree polynomial to plot

Once the equation is found, simply add other parameters to calibrate to the specific use. For example, if you have a 40 MHz that needs to pulse at a frequency of 10 MHz, simply divide the whole equation by 4. After all the calibrations have been applied, the equation can now be used with a thermometer to determine the 

Here's a sample program that could run on a microcontroller to correct the signal to accurately produce a 1 Hz signal at varying temperatures (using the equation for our crystal).

```c
// Pin to read temperature from
int TEMP = A0;

// Correction equation
float correction (x) {
	return -8.75*(10**(-12))*(x**3) +
				1.097*(10**(-9))*(x**2) - 
				4.582*(10**(-8))*x +
				6.081*(10**(-7))
}

void loop() {
    // Sample correction equation
    int correct = correction(TEMP);
    
    // Pulse every 1000 ms plus our correction
    delay(1000 + correct);
}
```
<br>
###Issues
Though everything worked very well in the tests, there are a few major flaws. The first was that due to a lack of both funding and time, I was only able to test the hot end of the spectrum. Ideally, test would've been preformed at very cold, cold, neutral, hot, and very hot temperatures, to verify the equation works well in all cases. Another issue that came up in testing was the fact that I was unable to load the correction equation onto the Arduino. I made the mistake of ordering Chinese clones instead of genuine Arduinos, and the clones simply weren't made well enough to handle an accurate measurement like that (there is very little documentation for the Arduino clone I had, but I believe the memory kept overflowing because it was trying to make too many calculations too fast). Another issue is that I was only able to work with a single type of quartz crystal. Given more time, I'd like to preform the experiment with other piezoelectric materials, such as ceramics and other kinds of crystal. All the advanced math functions used in this paper such as the Savitsky-Golay filter and the polynomial fitting were based on scientific libraries such as SciPy and numpy, since I did not have time to write them from scratch myself, though I would've liked to. Finally, there was not enough time to dive into all the tiny aspects of quartz crystals, such as cut and axis.

###The Future
Looking forward, this method could easily deployed in practical application quite well. An ATMega328p combined with a crystal quartz oscillator and a thermistor could fit on the tip of a finger, run off 3.3V, and cost less than $3 to manufacture at even a small scale. A tool for calibrating the circuit could be produced for \$20, though most workshops already have the required tools to do it without any extra equipment. If needed, duplicates of this circuit could be chained together and could calibrate each other, which could be useful for machines that humans cannot access. On board of large machinery that requires multiple separate timing circuits, there could be a single main timing circuit with a regulated temperature, that calibrates all the smaller circuits. 

---
###Bibliography
Please note that all files relating to this paper, such as code, data, and pictures, are freely available at <https://gitlab.com/jacksarick/ExtendedEssay/tree/master>

Shoaee, S.; Briscoe, J.; Durrant, J. R.; Dunn, S. (2013). "Acoustic Enhancement of Polymer/ZnO Nanorod Photovoltaic Device Performance" doi:10.1002/adma.201303304

Frerking, Marvin E. "Temperature Compensation." Crystal Oscillator Design and Temperature Compensation (1978): 130-76. doi:10.1007/978-94-011-6056-8

Matthys, Robert J. "Design of Crystal Oscillator Circuits." Analog Circuit Design (1991): 333-47. ISBN: 0894645528, 9780894645525

Bottom, Virgil E. "Introduction to quartz crystal unit design." Van Nostrand Reinhold electrical computer science and engineering series. Van Nostrand Reinhold, 1982. Web. 09 May 2016. ISBN: 0442262019, 9780442262013

Esterline, John. "The Future of Temperature-Compensated Crystal Oscillators." Circuit Cellar. Circuit Cellar, 26 Sept. 2014. Web. 21 Apr. 2016.


[^ubalance]: http://www.thinksrs.com/products/QCM200.htm
[^straing]: https://www.thorlabs.com/thorproduct.cfm?partnumber=PZS001&gclid=CIG21oudhcwCFVVZhgodvGsH7A
[^LASER]: https://www.thorlabs.com/thorproduct.cfm?partnumber=ASM003&gclid=CKXmo4SehcwCFYdehgodyXgCnw
[^crystal_1]: <img style="width:50%" src="./pics/xtal_1.JPG">
[^oscillator_1]: <img style="width:50%" src="./pics/oscl_1.JPG">
[^crystal_2]: <img style="width:50%" src="./pics/xtal_2.JPG">
[^oscillator_2]: <img style="width:50%" src="./pics/oscl_2.JPG">
[^curie]: Joliot, F. (1951). "Paul Langevin. 1872-1946". Obituary Notices of Fellows of the Royal Society  doi:10.1098/rsbm.1951.0009
[^crystal_mm]: Measurements are accurate to within .5 mm. Approximate surface area is about 38.4mm^2
[^rest_pic]: <img style="width:50%" src="./pics/rest_test_1.JPG">
[^rest_temp]: <img style="width:50%" src="./pics/rest_test_2.JPG">
[^digiocean]: https://www.digitalocean.com/
[^table]: Cheap/Expensive are measures of resources used on the computer. Fast/Slow are measures of computation time. Complexity is how easy they are to understand. Accuracy are how close they stay to the original data.
[^scipy]: http://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html
[^probe]: http://www.vernier.com/products/sensors/potential difference-probes/dvp-bta/
[^ard_time]: <img src="./pics/arduino-uno-r3-schematic.png"> arrow points to crystal
[^future]: Shoaee, S.; Briscoe, J.; Durrant, J. R.; Dunn, S. (2013). "Acoustic Enhancement of Polymer/ZnO Nanorod Photovoltaic Device Performance" doi:10.1002/adma.201303304
[^mad_science]: http://3.14.by/en/read/arduino-liquid-nitrogen-overclocking
[^wikipedia]: https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter
[^history]: http://www.piezo.com/tech4history.html
[^numpy]: http://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.polyfit.html
