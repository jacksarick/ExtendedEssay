#EE Notes
You're doing a physics EE (in the books), and your advisor is Mr. Olds. **Don't get lost**
###Things to remember
- Question come before experiment
- Stay on topic
- Stay in one field

##Your EE Ideas
- Sterling engine
- Logic gates
- Transistors
- Tensile strength of rope
- Soft Robotics
- Door knobs (seems like a topic with a lot of possibilities)
- Microcontrollers
- Chain mail (not really sure where I was going with this)
- Joule thiefs
- RADAR
- Magnet ID
- Acoustics (we're doing waves in class, so I felt obligated to write it down)