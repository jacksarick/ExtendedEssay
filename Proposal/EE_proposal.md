#Extended Essay Proposal
Student: Jack Sarick IB1   
Advisor: Kevin Olds

##Physics Research Question
**What is the relationship between temperature and the frequency a piezoelectric material (most likely a parabolic quartz crystal) vibrates at? What can be done to counteract this effect?**

The research question is a little broad, simply because I still don't know exactly what resources I'll have access to, but the topic is very interesting. Piezoelectricity, and more specifically crystal oscillators, are a very interesting field that is quite prevalent in our day to day lives. It ties in quite nicely to my interest in micro-controllers, as they function as a timer in most modern circuits. The second half of the question is a very real issue that is currently solved by control of the surrounding temperature, but I think that there is a better way. The question will need to be modified slightly as I learn more about the field, an example change would be "What is the relation between temperature and frequency in different piezoelectric materials?" allowing me to include both ceramics, crystals, and synthetic crystals in the study. Overall, this is a good research question because it is within my capabilities, there is enough information to work with, though it is not fully solved, the topic fits squarely in the category of physics, and, most importantly, I'm interested in it.

##Bibliography
Matsumoto, Takashi. Crystal Oscillator. Nihon Dempa Kogyo Co Ltd, assignee. Patent 20050088251A1. 21 Oct. 2004. Print.

Other patents (from google patents, checked against US patent database as of 01/26/16): US4099078A, US5808397A, US20040085147A1, US20040095044A1

Amos, S. W.; Roger Amos (2002). Newnes Dictionary of Electronics, 4th Ed.

Laplante, Phillip A. (1999). Comprehensive Dictionary of Electrical Engineering