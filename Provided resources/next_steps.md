#Next Steps for your EE Research Plan
>
> **You may feel that you have already done some of what follows, and
> that’s great if so. But consider this list of next steps carefully to
> see if there are ways in which you can improve.**

##The Question

-   What *exactly* are you evaluating?

-   By what criteria?

-   Can you state the main ideas in several different ways? Can you
    break them down?

-   Can you paraphrase your research question in order to explain to
    your ideas and research plan to your supervisor?

-   Do you know how to find varied opinions on the subject of your
    research question? What might those opinions be?

-   Where appropriate, have you learned about methodologies, terminology
    and theory in your area? Have you considered those in developing
    your question?

> **The Sources**

-   Did you choose the sources deliberately *in support of your research
    question* or just as items that were “on the shelf/internet” that
    reflected the topic?

-   What sources are needed to support *your argument* (even your broad
    ideas at this stage) and where can they be accessed?

-   Will there be any obstacles in obtaining the material? (ie. Time,
    location, language)

-   Are the authors experts identified by you or just “what’s out
    there”?

-   Did you consult some reference encyclopedias in your specific area
    for recommended reading?

-   Did you search across a wide range of subject-specific databases in
    the relevant fields? Or just one or two with which you are familiar?

-   Did you find out which *subject terms* to use in library catalogues
    or in databases for your research?

-   If you used the Internet did you try subject-specific Search Engines
    as recommended by the library? Did you use Google Scholar?

-   What were you *unable* to find? Would it be useful to ask a
    librarian for help?

> **A tightly focused research question and thesis statement will result
> from employing varied research strategies, asking questions and from
> in–depth analytical consideration of your sources.**
>
> **The librarians at school are happy to help you one-on-one with this
> research. Please make an appointment with Ms. R via email if you wish
> some further assistance.**
> [**mroughneen@ucc.on.ca**](mailto:mroughneen@ucc.on.ca)
