<style>
body {
	padding: 0px
}

.main {
  display: table;
  width: 100%;
  height: 100vh;
  text-align: center;
}

.center {
  display: table-cell;
  vertical-align: middle;
}

.top_left {
	line-height: 130%;
	text-align: left;
	position: absolute;
	top:5%;
	left:5%;
}
</style>

<div class="main">
	<span class="center">
	<h3 style="font-size: 26px;">Stabilizing the frequency of a quartz crystal oscillator at non ideal temperatures</h3>
	3815 words
	</span>
	<span class="top_left">
		28/06/16<br>
		Physics<br>
		Mr. Olds<br>
		Upper Canada College<br>
	</span>
</div>