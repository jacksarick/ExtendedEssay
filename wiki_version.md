{{User sandbox}}
<!-- EDIT BELOW THIS LINE -->
= Can the frequency of a quartz crystal oscillator be stabilized at non-ideal temperatures =

==Abstract==
'''REWRITE''' This paper looks at the decay in frequency stability of a quartz crystal oscillator at non ideal temperatures, and how to counteract that effect without controlling temperature. As a piezoelectric material, in this case quartz crystal, is heated, the frequency it vibrates at becomes less and less regular. Because the decrease in frequency is not the same across all crystals, it can be hard to correct accurately. Correcting the frequency of individual crystals has been abandoned in favour of using multiple crystals and controlling their frequency quite carefully. As an alternative approach, it is possible to use a third-degree polynomial to produce a more regular signal.

==Introduction==
Piezoelectricity /piˌeɪzoʊˌilɛkˈtrɪsɪti/ is the effect observed when certain materials produce an electric charge when mechanical force is applied. It is also a reversible effect, meaning that a piezoelectric material will produce a mechanical force in response to an applied electric field. In most cases, the mechanical force generated tends to be in the form of a vibration. Bone, some ceramics, most crystals, and DNA are all notable piezoelectric materials that show up in day-to-day life. Despite it's fascinating properties, piezoelectricity didn't get any attention from the scientific community when it was discovered in 1880 by the Curie brothers. Only in World War I was there a practical application for piezoelectric materials, when Paul Langevin (Pierre Curie was his doctoral advisor<ref>{{Cite journal|last=Joliot|first=F.|date=1951-11-01|title=Paul Langevin. 1872-1946|url=http://rsbm.royalsocietypublishing.org/cgi/doi/10.1098/rsbm.1951.0009|journal=Obituary Notices of Fellows of the Royal Society|language=en|volume=7|issue=20|pages=405–419|doi=10.1098/rsbm.1951.0009|issn=1479-571X}}</ref>) used quartz crystals to accurately detect submarines underwater<ref>{{Cite web|url=http://www.piezo.com/tech4history.html|title=Piezo Systems: History of Piezoelectricity|website=www.piezo.com|access-date=2016-06-23}}</ref>. Since then, about three hundred patents have been filed on the subject. Some of the ones I read and based my work off of were US3054966A, US3200349A, and US3581236A. All three patents deal with a similar problem, and helped as guides when I got stuck.

From WW1 to now, the uses increased exponentially, with piezoceramics finding their way into every microphone, radio, and car across the world. The practical application of this effect are virtually endless. Its high precision lends itself to delicate sensors such as microbalances<ref>{{Cite web|url=http://www.thinksrs.com/products/QCM200.htm|title=Quartz Crystal Microbalance - QCM200|website=www.thinksrs.com|access-date=2016-06-23}}</ref> for measuring mass and viscosity, and strain gauges<ref>{{Cite web|url=https://www.thorlabs.com/thorproduct.cfm?partnumber=PZS001&gclid=CIG21oudhcwCFVVZhgodvGsH7A|title=Thorlabs - PZS001 PZS001 - Piezo Stack With Strain Gauge Fitted|website=www.thorlabs.com|access-date=2016-06-23}}</ref> for measuring how much force is being applied to surface. A piezoelectric motor moves in tiny increments making it quite accurate, though they still generate enough force to move large objects (relative to their tiny size). There are hundreds more applications for this effect, and more being discovered regularly. Most people know a much more common application, so common I'm willing to bet that if you wear a watch, it's on your wrist. Quartz watches are named for the sliver for quartz crystal that keeps the time inside them. For the most part, they keep a very precise tempo, oscillating anywhere from 4 million times per second to 60 million depending on the circuit. At room temperature running for a decade straight, a quartz watch will normally be about a third of a second off. This makes them great for consumer technology that is designed to remain room temperature, such as a watch. 

The piezoelectric effect happens on quite a small scale. When a quartz crystal has no forces acting on it, it's charge is neutral, but it's internal structure is not uniform. As soon as a charge is run through the crystal, it's internal structure must move to rearrange itself so that it has a neutral charge. This small internal movement translates to the greater overall vibration of the crystal.

There are always edge cases, however. In hot environments the piezoelectric effect becomes less consistent, so a baker or welder may lose about 3 seconds every year if they wear their watch to work. In an electrical circuit where the timing circuit is too close to a hot component, it could lose 3 minutes a year, or more if the circuit is left to run for extended periods of time. On the other end of the spectrum, a quartz crystal cooled to -200º will oscillate approximately 325% faster<ref>{{Cite web|url=http://3.14.by/en/read/arduino-liquid-nitrogen-overclocking|title=Overclocking Arduino with liquid nitrogen cooling. 20⇒65.3Mhz @-196°C/-320°F : Svarichevsky Mikhail|website=3.14.by|access-date=2016-06-23}}</ref>. These cases may seem unlikely or unnoticeably small, but consider the extremes. On board every submarine, spaceship, and snowplow is a small device to keep time. It cannot be mechanical because it needs to last for a while without maintenance, yet it is difficult to make a small, cheap, low energy timing circuit that works and different temperatures. This means that correcting the decay in frequency is impossible by simply modulating the current or potential difference across the crystal. A mechanical solution would fail too, as engineering something that small and precise is not currently possible. This leads to the question: Is it possible to stabilize the frequency of an individual crystal oscillator without controlling the temperature of it?
 
===Hypothesis===
By measuring the frequency of a crystal at multiple temperatures, it is possible to generate an equation that can accurately correct the frequency based on the temperature of the crystal. My guess is that the equation will be linear, as heating and cooling the crystal will have opposite effects. This means that creating an equation to stabilize the crystal should be manageable. 


==Investigation==

Before attempting to run the full experiment, I first ran a series of small experiments to verify everything was functioning the way it should. The first of many small experiments was to make sure to the crystals produced the correct signal. To do this I simply wired a low-profile 4 MHz crystal to the y-axis of a 100 MHz oscillator<ref name=":0">Images can be found ay <nowiki>http://sarick.tech/imgs</nowiki></ref>. This produced a very nice sine wave, with a little background noise from the oscillator<ref name=":0" />. In an attempt to reduce this, I used an external power source to generate the current<ref name=":0" />, but it didn't really help, because I couldn't correctly calibrate the oscillator to deal with it.

The temperature of the crystal will need to be kept at a constant temperature, but that does not require the surrounding environment to be kept at a constant temperature to get an accurate reading. Other factors such as light, humidity, and pressure do not meaningfully impact the result. There are two ways to set up the experiment, depending on the resources at hand. With a good oscilloscope that has the capability of recording data, it is possible to record the output of the crystal directly. Unfortunately, an oscilloscope like that tends to cost $500 at the very least. Instead, a timing circuit built around the crystal designed to emit a regular signal based on the crystal's time. The circuit, along with an interface to connect it to a computer, can be made for around $15. There are two ways to run this experiment, depending on the frequency of the recording equipment.

===Setup with high-frequency equipment===
# Run some amount of current through the crystal (depends on kind of crystal)
# Affix a heating element to the crystal
# Log the potential difference across crystal using equipment that has a higher recording frequency than the crystal 
# Turn the heating element on and let it warm up
# Log the frequency again

===Setup with low-frequency equipment===
# Build a circuit that uses a crystal as the timer, to reduce the frequency.
# Program the circuit to pulse at a regular interval
# Affix a heating element to the crystal
# Record output of circuit using equipment operating that has a higher frequency and is more accurate
# Turn the heating element on and let it warm up
# Log the frequency again

A desireable recording frequency is about is about one hundred times faster than the speed of the crystal, so a 4 KHz crystal should be measured with equiment that has a resolution of 400 KHz. One hundred is a slightly arbitrary number, but too low, and 

The setup used was an Arduino attached to a Vernier™ differential potential difference probe<ref>{{Cite web|url=http://www.vernier.com/products/sensors/potential%20difference-probes/dvp-bta/|title=Differential Voltage Probe  >  Vernier Software & Technology|website=www.vernier.com|access-date=2016-06-23}}</ref>, recording the data with LoggerPro. An Arduino is an ATMega238p microcontroller on a board with a programmer, some IO pins, two ports for power and programming, but most importantly, it uses a 12 MHz quartz crystal oscillator for it's timing circuit<ref name=":0" />. 

The heating element to be sued, was a topic of discussion between the lab supervisor and myself. The on board crystal of the Arduino is 4.5mm x 9.5mm<ref>Measurements are accurate to within .5 mm. Approximate surface area is about 38.4mm^2</ref>, so I had to find a method that would be:
# Small enough to fit on the crystal
# Have an adjustable temperature
# Maintain a constant temperature once changed

My initial thought was to use the tip of a soldering iron, but it was much too hot, and would have desoldered parts of the Arduino. The winning idea ended up being fastening a resistor to the top of the crystal, and connect it to a variable DC power source. To determine the resistance needed, a large resistor (R1) in was put in parallel to a rheostat (R2), and measured the potential difference and current of the circuit, as shown in I-1<ref name=":0" />.
[[File:Diagram of Resistors.jpg|thumb|center|upright=2.0|I-1: A diagram of the circuit used to calibrate the heating resistor]]
From there, a temperature probe was wrapped around resistor (R1). I chose to use an infrared thermometer to measure the surrounding temperature, because our model could measure from the infrared sensor and the probe simultaneously (and it was really fun to use). Through a bit of trial and error (there were also very few resistors to choose from), a 46Ω±10% resistor was chosen. When it was the only load in the circuit, it heated up approximately 10ºC per volt.


===Experiment===
'''Write more stuff'''

There was very little uncertainty in this experiment, because there weren't many variables to measure. Measuring time was incredibly accurate. The internal clock of the computer is effectively perfect, being accurate to within<math>1 * 10^{-9}</math> seconds. I was sampling at 100 KHz times a second, and the Arduino was set to pulse at 1 KHz, so the furthest distance between a sample and the Arduino's voltage peaking is <math>5 * 10^{-7}</math> seconds (half of one millionth of a second), too small to make a meaningful impact. The only factor that could have uncertainty is the temperature, as the thermometer was accurate to within ±.1 degrees celcius. This uncertainty would normally be large, but in reality it makes little to no impact on the experiment. The thermometer is only used to record the difference between the temperatures, not the temperatures. Assuming that it is always inaccurate by a regular amount, which is a common error for measurement devices, it won't impact the end result in any noticeable way.

==Analysis==

Each trial ran for 5 seconds, sampling at 100,000 times a second. Since there were 9 trials <math>5 * 100,000 * 9 = 4,500,000</math> so there were 4 million data points after all the garbage data had been pared out. To change the data from a set of potential difference across the Arduino vs. time is conceptually easy, but hard in practice. In practice, the data isn't 
[[File:Trial 3 0V@26.5 Raw Data.png|thumb|center|upright=2.0|A-1: A graph of potential difference across the Arduino vs. time. This was the data that was collected by the computer]]
The information is simply too dense to see anything, though there are is one small feature that is visible right from the start. Underneath the large potential difference changes, a faint sub-pattern is visible. This sub-pattern is only seen when zoomed quite far out from the graph and doesn't show up again in any of the later data. I don't believe further investigation would reveal it to be anything except an optical illusion.

Attempting to smooth out the raw data was a mistake. I still documented the whole process because it did reveal some other interesting features about the data. The following were my  at the time: The sensor used was quite accurate, both in timing and in data. The Arduino, however, was not accurate, and had trouble with rapid potential difference changes. Since the entire experiment was all about potential difference changes, the data collected was too noisy to work with. If it had been a straight line, or even a parabola, LoggerPro or Excel could've used a simple line-of-best-fit algorithm, but this was a sine wave. There were three algorithms that can be used for smoothing out this kind of data:
# Convolution
# Discrete Fourier Transformation (DFT or FFT)
# Savitsky-Golay Filter (SavGol)
In an effort to decide which one to use, a table of pros and cons was made for each of the smoothing methods<ref>Cheap/Expensive are measures of resources used on the computer. Fast/Slow are measures of computation time. Complexity is how easy they are to understand. Accuracy are how close they stay to the original data.</ref>
{| class="wikitable"
!Convolution
!DFT
!SavGol|SavGol
|-
|'''Pros:''' 
Cheap

Simplest
|'''Pros:''' 
Fast

Smooth
|'''Pros:''' 
Accurate

Smooth
|-
|'''Cons:''' 
Slow

Not Smooth
|'''Cons:''' 
Complex

Inaccurate
|'''Cons:''' 
Slow

Complex 

Expensive
|}

As a test, a sample of data was run through each method.
[[File:Comparison of methodss.png|thumb|center|upright=2.0|A-2: Side-by-side comparison of all four data smoothing methods]]
Between the pro-cons table and the comparison, choosing was easy. DFT was fast, and produced a very smooth curve, yet it had to dip below the lowest data point, and couldn't go above it. Between he Savitsky-Golay and convolution methods, I went with Savitzky–Golay because it produced a nicer curve. Despite being the slowest, it was the one settled on. <blockquote>A Savitzky–Golay filter is a digital filter that can be applied to a set of digital data points for the purpose of smoothing the data, that ... This is achieved, in a process known as convolution, by fitting successive sub-sets of adjacent data points with a low-degree polynomial by the method of linear least squares.<ref>{{Cite journal|title=Bad title|url=https://en.wikipedia.org/wiki/Savitzky%25E2%2580%2593Golay_filter|journal=Wikipedia, the free encyclopedia|language=en}}</ref></blockquote>Note that it does use convolution, but it uses it in parallel with a series of other equations. Both Savitzky-Golay and convolution relay heavily on low-degree polynomials, something that comes up later in the paper. This does explain why the Convolution and Savitzky-Golay plots look similar. In both filters, the goal is to find a low-degree polynomial that fits a small window of the data, however the Savitzky-Golay filter later joins the smaller equations together, while convolution does not. I am not even close to qualified to write my own Savitzky-Golay filter, so I used SciPy's function<ref>{{Cite web|url=http://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html|title=scipy.signal.savgol_filter — SciPy v0.19.0.dev0+ce24da1 Reference Guide|website=scipy.github.io|access-date=2016-06-23}}</ref>. All of the code for it is open source, so I double checked it and it was exactly what I needed. Here is the smoothed out version of the above graph. 
[[File:Trial 3 0V@26.5 Savitzky-Golay Filter.png|thumb|center|upright=2.0|A-3: The raw data after being passed through the Savitsky-Golay Filter]]<br>Unsurprisingly, it looks almost exactly the same as Fig. A-1. The data that was being smoothed out was tiny, so small the computer had trouble graphing it. That fact, combined with the sheer mass of data, means that it is impossible to visualize any meaningful data. The only way I was able to interface the data to a human being was to convert it to sound. If you listened closely, you could hear the imperfections in the sound, but it was too grating on the ears to listen for more than four or five seconds. 

On reflection, the problem smoothing the graph with a Savitzky-Golay filter, or most other filters, is the fact that they smooth out the data too much. In the case of Savitzky-Golay, it generates a series of equations that fit a point and it's neighbours well. Since all of the data was quite similar and computing power was relatively low, the filter produced curves too similar to detect a difference in.

Since smoothing the data out at the start did not work, another approach was needed. Human beings tend to be really good at seeing things, especially seeing patterns like frequency. Unfortunately, the data included a quarter of a million peaks and troughs, so it would take a while to record all of them if a human did it. Computers don't have the ability to do this by themselves. To detect the peaks and troughs in the rough data, a small bit of code was used to pull any point where the three points on either side of it were all above or below it. The logical representation would be similar to<math display="inline">((p > n) \bigwedge (p > k)) \bigvee ((p < n) \bigwedge (p < k))</math> where `p` is the test point, and `n` and `k` are the points on either side of `p`. This creates a set of data that represents the actual frequency of the crystal. Taking the average of the distance between the peaks and troughs yields average frequency. To turn it into time lost/gained per second, simply divide it by the time the given trial ran for.
[[File:IDFK peak trough.png|thumb|center|upright=2.0|A-4: The distance in time between each peak trough-set.]]
There appears to be some sort of pattern that repeats at a regular interval (about once every 10,000 pairs), but it is too faint to actually determine the pattern and as a result, too difficult determine its source. My best guess is it that it has something to do with the sample rate of the probe overlapping with the frequency of the crystal.

Now that the average time difference has been calculated, it is trivial to plot difference in time against temperature to see the decline in frequency stability. The blue line is the data, the red line is "ideal" (frequency is the same regardless of temperature).  
[[File:Average Time Lost per Second.png|thumb|center|upright=2.0|A-5: Average time lost per second is the amount of time that the timing circuit was wrong by at each temperature]]
It's not a perfect curve by any measure, but it fits as well as can be expected. Looking at the graph, the decline doesn't look linear, which disproves my original theory. It has a pretty clear curve. It could techinically be logarithmic, but nothing else would seem to suggest that. This means that the graph of the frequency is going to be a polynomial of the second degree or higher, but because the trend never changes direction it has to be a third degree polynomial. Once the degree of the equation is known, a curve can easily be fit to the data. In this case, numpy<ref>{{Cite web|url=http://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.polyfit.html|title=numpy.polyfit — NumPy v1.10 Manual|website=docs.scipy.org|access-date=2016-06-23}}</ref> (another library for Python) was the tool of choice for polynomial fitting, because polynomial interpolation is difficult to do without a background in math. For this graph, where `x` is temperature and `y` is time offset, the equation looks like this:

<math>f(x) =(-8.75*10^{-12} *x^3) + (1.097*10^{-9}*x^2) - (4.582*10^{-8}*x) + 6.081*10^{-7}</math>

Plotting it on top of our data, it is quite a good fit, and accurately reflects what was predicted, that the data fits on a third degree polynomial. The collected data looks a bit flat, but this is because of the lack of data points.
[[File:Curve fit graph for me.png|thumb|center|upright=2.0|A-6: Data collected graph on top of the generated equation]]
As expected, the extremes of heat and cold are in opposite directions, indicating a loss of time in the cold direction (frequency speeding up), and a gaining of time in the hot direction (frequency slowing down). Verifying that the curve and the data look similar shows that correcting the data with that equation returns a correct measurement of time. Running the data back through the equation produces a flat line, proving that the signal is constant.

It is interesting to note that the polynomial fit function implemented is the same one used in the Savitzky-Golay filter. The Savitzky-Golay was by all means a mistake on my part, it wasn't totally misguided. My mistake was thinking that trying to smooth the data at A Savitzky-Golay could theoretically be applied as an alternative to a 3rd degree polynomial, and it might even do a better job in some cases. For it to do a better job than a polynomial would require a much longer calibration process, and would drastically increase the memory requirements of the system processing the crystal's signal.

The method used to generate the stabilization equation can be used for any piezoelectric material, from simple ceramics to complex crystals 
# Measure material against stable timer three temperatures far enough apart to create an equation with 
# Find average peak-trough difference 
# Plot peak-trough against temperature 
# Fit 3rd degree polynomial to plot 
Once the equation is found, simply add other parameters to calibrate to the specific use. For example, if you have a 40 MHz that needs to pulse at a frequency of 10 MHz, simply divide the whole equation by 4. After all the calibrations have been applied, the equation can now be used with a thermometer to determine the 

Here's a sample, quite basic, program that could run on a microcontroller to correct the signal to accurately produce a 1 Hz signal at varying temperatures (using the equation for our crystal).<syntaxhighlight lang="arduino">
// Written in Arduino C, based on C/C++
// Pin to read temperature from
int TEMP = A0;

// Correction equation
float correction (x) {
	return -8.75*(10**(-12))*(x**3) +
		1.097*(10**(-9))*(x**2) - 
		4.582*(10**(-8))*x +
		6.081*(10**(-7))
}

void loop() {
    // Sample correction equation
    int correct = correction(TEMP);
    
    // Pulse every 1000 ms plus our correction
    delay(1000 + correct);
}
</syntaxhighlight>Though everything worked very well in the tests, there are a few major flaws. The first was that due to a lack of both funding and time, I was only able to test the hot end of the spectrum. Ideally, test would've been preformed at very cold, cold, neutral, hot, and very hot temperatures, to verify the equation works well in all cases. Another issue that came up in testing was the fact that I was unable to load the correction equation onto the Arduino. I made the mistake of ordering Chinese clones instead of genuine Arduinos, and the clones simply weren't made well enough to handle an accurate measurement like that (there is very little documentation for the Arduino clone I had, but I believe the memory kept overflowing because it was trying to make too many calculations too fast). Another issue is that I was only able to work with a single type of quartz crystal. Given more time, I'd like to preform the experiment with other piezoelectric materials, such as ceramics and other kinds of crystal. All the advanced math functions used in this paper such as the Savitsky-Golay filter and the polynomial fitting were based on scientific libraries such as SciPy and numpy, since I did not have time to write them from scratch myself, though I would've liked to. Finally, there was not enough time to dive into all the tiny aspects of quartz crystals, such as cut and axis.

Looking forward, this method could easily be used in practical application quite well. An ATMega328p combined with a crystal quartz oscillator and a thermistor could fit on the tip of a finger, run off just 3.3V, and cost less than $3 to manufacture at even a small scale. A tool for calibrating the circuit could be produced for $20, though most workshops already have the required tools to do it without any extra equipment. If needed, duplicates of this circuit could be chained together and could calibrate each other, which could be useful for machines that humans cannot access. On board of large machinery that requires multiple separate timing circuits, there could be a single main timing circuit with a regulated temperature, that calibrates all the smaller circuits. 

== Bibliogrpahy ==
Please note that all files relating to this paper, such as code, data, and pictures, are freely available at <https://gitlab.com/jacksarick/ExtendedEssay/tree/master

Bangert, Richard H. Crystal Controlled Oscillator with Temperature Compensation. Bendix Corp, assignee. Patent US3200349A. 05 Feb. 1963. Print.

Berman, Leon. High Stability Oscillator. Cit Alcatel, assignee. Patent US3581236A. 08 Jan. 1968. Print.

Ralph, Etherington. Crystal Controlled Oscillator with Temperature Compensating Means. Gen Electric, assignee. Patent US3054966A. 15 July 59. Print.

Shoaee, S.; Briscoe, J.; Durrant, J. R.; Dunn, S. (2013). "Acoustic Enhancement of Polymer/ZnO Nanorod Photovoltaic Device Performance" doi:10.1002/adma.201303304

Frerking, Marvin E. "Temperature Compensation." Crystal Oscillator Design and Temperature Compensation (1978): 130-76. doi:10.1007/978-94-011-6056-8

Matthys, Robert J. "Design of Crystal Oscillator Circuits." Analog Circuit Design (1991): 333-47. ISBN: 0894645528, 9780894645525

Bottom, Virgil E. "Introduction to quartz crystal unit design." Van Nostrand Reinhold electrical computer science and engineering series. Van Nostrand Reinhold, 1982. Web. 09 May 2016. ISBN: 0442262019, 9780442262013

Esterline, John. "The Future of Temperature-Compensated Crystal Oscillators." Circuit Cellar. Circuit Cellar, 26 Sept. 2014. Web. 21 Apr. 2016.
